import { initializeApp } from 'https://www.gstatic.com/firebasejs/10.4.0/firebase-app.js';
import { getDatabase, ref, push } from 'https://www.gstatic.com/firebasejs/10.4.0/firebase-database.js';

const firebaseConfig = {
    apiKey: "AIzaSyAypjZRP8CVEHHjWsXZNBt2n1GuTIE7_b4",
    authDomain: "proyectofinal-9bd20.firebaseapp.com",
    projectId: "proyectofinal-9bd20",
    storageBucket: "proyectofinal-9bd20.appspot.com",
    messagingSenderId: "489970235282",
    appId: "1:489970235282:web:beeb5c8ecd332d4928334b"
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);

// Obtiene la referencia al formulario y la sección de productos en la base de datos
const productForm = document.getElementById('product-form');
const productosRef = ref(database, 'Producto');

// Variable para controlar si el formulario está siendo procesado
let formProcessing = false;

// Evento de submit del formulario
productForm.addEventListener('submit', function (p) {
    p.preventDefault();

    // Si el formulario ya está siendo procesado, no hacer nada
    if (formProcessing) {
        return;
    }

    // Activar el indicador de procesamiento
    formProcessing = true;

    // Obtiene los valores de los campos de entrada
    const imgUrl = document.getElementById('img-url').value;
    const nombre = document.getElementById('nombre').value;
    const descripcion = document.getElementById('descripcion').value;

    // Añade un nuevo producto a la base de datos
    push(productosRef, {
        Imagen: imgUrl,
        Nombre: nombre,
        Descripcion: descripcion
    })
        .then(() => {
            // Limpia los campos del formulario después de agregar el producto
            document.getElementById('img-url').value = '';
            document.getElementById('nombre').value = '';
            document.getElementById('descripcion').value = '';

            // Cierra el modal
            const productModal = new bootstrap.Modal(document.getElementById('productModal'));
            productModal.hide();
        })
        .catch((error) => {
            console.error('Error al agregar el producto: ', error);
        })
        .finally(() => {
            // Desactivar el indicador de procesamiento
            formProcessing = false;
        });
});
