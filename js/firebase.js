  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
  import { getAuth } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js"
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration

  const firebaseConfig = {
    apiKey: "AIzaSyAypjZRP8CVEHHjWsXZNBt2n1GuTIE7_b4",
    authDomain: "proyectofinal-9bd20.firebaseapp.com",
    projectId: "proyectofinal-9bd20",
    storageBucket: "proyectofinal-9bd20.appspot.com",
    messagingSenderId: "489970235282",
    appId: "1:489970235282:web:beeb5c8ecd332d4928334b"
  };

  // Initialize Firebase
  export const app = initializeApp(firebaseConfig);
  export const auth = getAuth(app);